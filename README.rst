.. -*- restructuredtext -*-

=========================
*Persistent Search Trees*
=========================
Presentation for the project of INFO-F413 *Data structures and algorithms* (ULB).

From the article
*Planar Point Location Using Persistent Search Trees*
of Neil Sarnak and  Robert E. Tarjan.

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



|Persistent Search Trees|

.. |Persistent Search Trees| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/13/1273575130-4-persistent-search-trees-logo_avatar.png
